----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2019 00:06:37
-- Design Name: 
-- Module Name: Debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Debouncer is
port (
 clk : in std_logic;
 rst : in std_logic;
 btn_in : in std_logic;
 btn_out : out std_logic);
end Debouncer;

architecture Behavioral of Debouncer is

signal Q1, Q2, Q3 : std_logic;
BEGIN
 process(clk)
 begin
if (clk'event and clk = '1') then
  if (rst = '0') then
  Q1 <= '0';
  Q2 <= '0';
  Q3 <= '0';
  else
  Q1 <= btn_in;
  Q2 <= Q1;
  Q3 <= Q2;
  end if;
  end if;
  end process;
  btn_out <= Q1 and Q2 and (not Q3);


end Behavioral;
