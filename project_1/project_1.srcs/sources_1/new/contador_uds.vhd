----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2019 00:13:09
-- Design Name: 
-- Module Name: count_uds - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity contador_nbits is
generic(
  nbits : positive        := 4;
  limit: std_logic_vector := x"2F"
  );
 Port (
 clk: in std_logic;
 counting_way: in std_logic;
 
 enable: in std_logic;
 reset: in std_logic;
 count: out std_logic_vector(nbits-1 downto 0)
  );
end contador_nbits;

architecture Behavioral of contador_nbits is

--TYPE ESTATE_T is (S0,S1,S2,S3,S4,S5,S6,S7,S8,S9);
signal estate_register:std_logic_vector(nbits-1 downto 0); 
signal next_estate:std_logic_vector(nbits-1 downto 0);

constant zeros : std_logic_vector(nbits - 1 downto 0) := (others => '0');
begin
 sr:process(clk,reset,enable)
  begin
   if reset='1' then 
   estate_register<=(others=>'0');
   if enable='1' then
    elsif rising_edge(clk) then
    estate_register<=next_estate;
    end if;
    end if;
   end process;
   comb:process(enable,estate_register,counting_way)
    begin
    if enable='1' then
    --Sentido de la cuenta ascendente (incremental)
                    if counting_way = '1' then
                        
                        if estate_register = limit then 
                        next_estate <= (others => '0'); --Se prioriza que la cuenta llegue al limit para reiniciarla a cero.
                        else 
                        next_estate <= estate_register + 1;      --En caso contrario, se aumenta en una unidad.
                        end if;
                    
                    elsif counting_way = '0' then				
                           if estate_register = zeros then 
                           next_estate<= limit; --Se prioriza que la cuenta llegue a cero para reiniciarla al valor limit.
                                else 
                                next_estate <= estate_register - 1;       --En caso contrario, se disminuye en una unidad.
                                        end if;
                     end if;
    else
    next_estate<=estate_register;
    end if;
  
           
     end process ;

count<=next_estate;
end Behavioral;
