----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2019 14:27:36
-- Design Name: 
-- Module Name: Contador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Contador is
 Port ( 
 clk: in std_logic;
 rst:in std_logic;
 pausa: in std_logic;
 uds:out std_logic_vector(3 downto 0)
 );
end Contador;

architecture Behavioral of Contador is
COMPONENT contador_nbits
generic(
n_bits:positive:=4
);
PORT(
  clk: in std_logic;
  reset: in std_logic;
  enable: in std_logic;
  counting_way: in std_logic;
  count: out std_logic_vector(3 downto 0)
  );
  END COMPONENT;
  
  signal PLAY: std_logic;
  signal Counting: std_logic;

begin

PLAY<=not pausa;

cnt_uds:contador_nbits
  PORT MAP(
   reset=>rst,
   clk=>clk,
   enable=>PLAY,
   counting_way=>Counting,
   count=>uds
   );

end Behavioral;
