----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2019 23:37:25
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
 Port (
 RESET: in std_logic;
 CLOCK: in std_logic;
 PAUSA: in std_logic;
 button_enable_count : in STD_LOGIC; 
 button_counting_way : in STD_LOGIC;  --boton para el sentido de la cuenta
 DISPLAY_NUM: out std_logic_vector(6 downto 0);
 DISPLAY_SEL: out std_logic_vector(7 downto 0);
 LEDS: out std_logic_vector(1 downto 0)
  );
end top;

architecture behavioral of top is

COMPONENT clk_divider
generic(
 f_in: in positive:=2; --frecuencia de entrada
 f_out: in positive:=1  --frecuencia de salida
 );
 PORT(
   clk_in:in std_logic;
   rst: in std_logic;
   clk_out: out std_logic
   );
   END COMPONENT;
   
   COMPONENT sincronizador
   PORT(
   sync_in: IN STD_LOGIC;
   clk: IN STD_LOGIC;
   sync_out: OUT STD_LOGIC
   );
   END COMPONENT;
   
   COMPONENT debouncer
   PORT(
   clk : in std_logic;
    rst : in std_logic;
    btn_in : in std_logic;
    btn_out : out std_logic
    );
    END COMPONENT;
    
    COMPONENT Contador
    PORT(
     rst: in std_logic;
     clk: in std_logic;
     pausa: in std_logic;
     uds: out std_logic_vector(3 downto 0);
     dec: out std_logic_vector(3 downto 0)
     
     
     );
     END COMPONENT;
     
     COMPONENT decoder
     PORT(
     code: in std_logic_vector(3 downto 0);
     led: out std_logic_vector(6 downto 0) 
     );
     END COMPONENT;
     
     COMPONENT display
     generic(  nsegments : positive := 7);
     
     PORT(
               clk  : in STD_LOGIC;
               segment_uds       : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
               segment_dec        : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
               display_number     : out STD_LOGIC_VECTOR (nsegments-1 downto 0);
               display_selection  : out STD_LOGIC_VECTOR (3 downto 0)
               );
      END COMPONENT;
      
      --SE�ALES INTERNAS
      signal CLK_1kHz: std_logic;
      signal CLK_170Hz: std_logic;
      signal CLK_1Hz: std_logic;
      
      signal pausa_sinc: std_logic;
      signal display_inter: std_logic_vector(1 downto 0);
      
      
      signal COUNT : STD_LOGIC_VECTOR (7 downto 0);
    
      --DECODIFICADOR DE UNIDADES
      signal led_unid : STD_LOGIC_VECTOR (6 downto 0);
      
      --DECODIFICADOR DE DECENAS
      signal led_dec : STD_LOGIC_VECTOR (6 downto 0);
      
      
      
begin

DISPLAY_SEL<="111111"&display_inter(1 downto 0);
LEDS<=PAUSA&RESET;



clk_div_sinc:clk_divider
 GENERIC MAP(
       f_in=>200000000,
       f_out=>1000
       )
  PORT MAP(
     clk_in=>CLOCK,
     rst=>RESET,
     clk_out=>CLK_1kHz
     );
 clk_div_count:clk_divider
 GENERIC MAP(
        f_in=>1000,
        f_out=>170
        )
   PORT MAP(
      clk_in=>CLK_1kHz,
      rst=>RESET,
      clk_out=>CLK_170Hz
      );
   clk_div_disp:clk_divider
   GENERIC MAP(
          f_in=>170,
          f_out=>1
          )
     PORT MAP(
        clk_in=>CLK_170Hz,
        rst=>RESET,
        clk_out=>CLK_1Hz
        );
   sinc_entradas:sincronizador
        PORT MAP(
        clk=>CLK_1kHz,
        sync_in=>PAUSA,
        sync_out=>pausa_sinc
        );
   cont:contador
     PORT MAP(
     rst=>RESET,
     clk=>CLK_1Hz,
     pausa=>pausa_sinc,
     uds=>COUNT(3 downto 0)
     );
     dcd_unid: decoder
         Port map( code =>COUNT (3 downto 0), 
                       led => led_unid);
                       
      dcd_dec: decoder
          Port map( code => COUNT (7 downto 4), 
                       led  => led_dec);
         
      display_refresh: display
          Port map( 
                  clk => CLK_170Hz,
                  segment_uds => led_unid,
                  segment_dec => led_dec,
                  display_number=> DISPLAY_NUM,    
                  display_selection => display_inter
                   );
                           

  
end Behavioral;
